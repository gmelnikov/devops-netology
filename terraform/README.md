# Terraform

## .gitignore
Игнорируются:
- Все файлы директории .terraform во всех вложенных директориях
```
**/.terraform/*)
```

- Все файлы, имена которых заканчиваются на .tfstate и содержат в имени *.tfstate.*
```
*.tfstate
*.tfstate.*
```

- Файл crash.log 
```
crash.log
```

- Все файлы, имена которых заканчиваются на .tfvars
```
*.tfvars
```

- Файл override.tf
```
override.tf
```

- Файл override.tf.json
```
override.tf.json
```

- Файлы, имена которых заканчиваются на _override.tf и _override.tf.json
```
*_override.tf
*_override.tf.json
```

- Файл .terraformrc
```
.terraformrc
```

- Файл terraform.rc
```
terraform.rc
```
